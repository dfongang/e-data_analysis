import os
import joblib
import pandas as pd


def load_input_data():
    dirpath = os.path.dirname(__file__)
    filepath = os.path.join(dirpath, '..', "data/input/supplier_car_valid.json")
    data = pd.read_json(path_or_buf=filepath
                        , orient='records'
                        , encoding='utf-8')
    data.rename(columns={'Attribute Names': 'AttributeNames', 'Attribute Values': 'AttributeValues'}, inplace=True)
    return data


def get_unique_ids(data):
    return list(data['ID'].unique())


def explode_data_attributes(data):
    first_attributes = ['ID', 'MakeText', 'TypeName', 'TypeNameFull', 'ModelText', 'ModelTypeText']
    final_dict = {}

    for ID in get_unique_ids(data):
        sub_data = data.loc[data['ID'] == ID]
        sub_dict = sub_data[first_attributes].mode().iloc[0, :].to_dict()

        # adding the second attributes
        sub_dict.update(dict(zip(sub_data.AttributeNames.values, sub_data.AttributeValues.values)))

        # updating the final data structure (a dictionary)
        final_dict.update({ID: sub_dict})
    data_exploded = pd.DataFrame(final_dict).transpose()
    return data_exploded


def create_preprocessed_data():
    data = load_input_data()
    data = explode_data_attributes(data)
    return data.replace({'null': None})


def pickle_preprocessed_data(output_dirpath, data):
    pkl_filepath = os.path.join(output_dirpath, 'preprocessed_data.pkl')
    joblib.dump(data, pkl_filepath)
    return pkl_filepath


def write_preprocessed_data(filepath, data):
    """ Write preprocessed data into the xlsx supplier output file."""
    with pd.ExcelWriter(filepath) as writer:
        data.to_excel(writer, sheet_name='preprocessed_data', index=False)








