# e-Data Transformation

## 0-) Looking at the input data 

The supplier input data is a json file, that contains a sequence of 21906 documents.
![supplier_car.json an invalid json file!](images/supplier_car_invalid.png)
__Validating the json file__

As it, the `supplier_car.json` is not a valid json file. 
Manually,
- make a copy  of `supplier_car.json` to `supplier_car_temp.json`. 
On the temporary file,
- add the opening `[` and  the closing `]` brakets respectively at the beginning and the end of the file,
- find all `}{` and replace each with `},{`.

From now, the json file content is an array or a list of 21906 elements, as shown in the picture below.
![supplier car: a valid json file!](images/supplier_car_valid_json.png)

The valid json file is renamed to `supplier_car_valid.json`.


## 1-) Pre-processing
### 1.1  Loading the input data

_function_: `load_input_data`.
The supplier input data is an array of 21906 entries, with 9 columns. 
Only the ID is of type integer, all others variables are string.

Is there one product per row ?

### 1.2 Singularity transformation
An inspection of the data shows there are 19 rows per ID, except the ID 824 that counts 18 rows.
Because there is 1153 unique IDs, it comes that 21906 = 1152*19 + 18.










