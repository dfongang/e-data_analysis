""" integration module """
import os
import joblib
import openpyxl
import pandas as pd


def load_normalised_data(pkl_filepath):
    """ Load normalised data """
    return joblib.load(pkl_filepath)


def make_integration(data):
    """ Make integration """

    mapping_dict = {
        'BodyTypeText': 'carType',
        'BodyColorText': 'color',
        'ConditionTypeText': 'condition',
        'City': 'city',
        'MakeText': 'make',
        'FirstRegYear': 'manufacture_year',
        'Km': 'mileage',
        'ModelText': 'model',
        'TypeName' : 'model_variant',
        'FirstRegMonth': 'manufacture_month'
    }

    # new attribute columns
    data['mileage_unit'] = 'kilometer'
    data['type'] = 'car'
    # are they all cars ? could we found a tank for instance ?
    data['fuel_consumption_unit'] = 'l_per_100_km'
    # the consumption value is not recorded. this is a new value.
    # Else, keep 'l_km_consumption' as in Target Data. Then non integrated consumption values should be divided by 100.

    # added_columns
    added_columns = ['mileage_unit', 'type', 'fuel_consumption_unit']
    keep_columns = list(mapping_dict.values()) + added_columns

    # renaming columns
    data.rename(columns=mapping_dict, inplace=True)

    # omitting all others columns
    data = data[keep_columns].copy(deep=True)

    # rename 'Occasion' to 'Used' from data['condition']
    data['condition'] = data['condition'].cat.rename_categories({'Occasion': 'Used'})
    return data


def pickle_integrated_data(output_dirpath, data):
    pkl_filepath = os.path.join(output_dirpath, 'integrated_data.pkl')
    joblib.dump(data, pkl_filepath)
    return pkl_filepath


def write_integrated_data(filepath, data):
    """ Write integrated data into the xlsx supplier output file."""
    _book = openpyxl.load_workbook(filepath)
    with pd.ExcelWriter(filepath, engine='openpyxl') as writer:
        writer.book = _book
        data.to_excel(writer, sheet_name='integrated_data', index=False, na_rep='')


