# ---------------------------------------------------------------------------------------------------------------------
#   E-Data Analysis
# ---------------------------------------------------------------------------------------------------------------------
import os
import pandas as pd

from preprocessing import create_preprocessed_data
from preprocessing import pickle_preprocessed_data
from preprocessing import write_preprocessed_data

from normalisation import normalise_data
from normalisation import pickle_normalised_data
from normalisation import write_normalised_data

from integration import make_integration
from integration import pickle_integrated_data
from integration import write_integrated_data

output_dirpath = "./data/output/"
output_dirpath = os.path.join(os.path.dirname(__file__), output_dirpath)

o_filename = "supplier_car_output.xlsx"


# Create an empty output file -
# ---------------------------------------------------------------------------------------------------------------------
def create_empty_output_file(o_filename, o_dirpath=output_dirpath):
    filepath = os.path.join(o_dirpath, o_filename)
    pd.DataFrame({}).to_excel(filepath)


create_empty_output_file(o_filename)


# - Data Preprocessing -
# ---------------------------------------------------------------------------------------------------------------------
data_preprocessed = create_preprocessed_data()

write_preprocessed_data(filepath=os.path.join(output_dirpath, o_filename),  data=data_preprocessed)
pkl_filepath_preprocessed = pickle_preprocessed_data(output_dirpath=output_dirpath, data=data_preprocessed)


# - Data Normalisation -
# ---------------------------------------------------------------------------------------------------------------------
data_normalised = data_preprocessed.copy(deep=True)
data_normalised = normalise_data(data=data_normalised)

write_normalised_data(filepath=os.path.join(output_dirpath, o_filename),  data=data_normalised)
pkl_filepath_normalised = pickle_normalised_data(output_dirpath=output_dirpath, data=data_normalised)


# - Data Integration -
# ---------------------------------------------------------------------------------------------------------------------
data_integrated = data_normalised.copy(deep=True)
data_integrated = make_integration(data=data_integrated)

write_integrated_data(filepath=os.path.join(output_dirpath, o_filename),  data=data_integrated)
pkl_filepath_integrated = pickle_integrated_data(output_dirpath=output_dirpath, data=data_integrated)