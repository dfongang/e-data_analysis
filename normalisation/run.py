import os

from normalisation import normalise_data
from normalisation import load_preprocessed_data


dirpath = os.path.dirname(__file__)

pkl_filepath = './data/output/preprocessed_data.pkl'
pkl_filepath = os.path.join(dirpath, '..', pkl_filepath)

data = load_preprocessed_data(pkl_filepath)
data = normalise_data(data)
data.info()

