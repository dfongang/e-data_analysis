"""
 normalisation module
"""
import os
import re
import joblib
import openpyxl
import numpy as np
import pandas as pd


def load_preprocessed_data( pkl_filepath):
    """ Load preprocessed data """
    return joblib.load(pkl_filepath)


def clean_string_lower_case(val):
    """
        Remove all characters except numbers and alphabets
        Return a string in lower case
    """
    return re.sub('[\W_]+', '', val).lower()


def normalise_MakeText(data):
    """ Normalise the attribute MakeText"""

    # special cases dictionary
    _make_dict = {
        'fordusa': 'Ford (USA)',
        'iso': 'Iso',
        'desoto': 'DeSoto',
        'ruf': 'Ruf',
        'delorean': 'DeLorean',
        'bmwalpina': 'Alpina',
        'mini': 'MINI',
    }

    def _cat_default_rule(cat_value):
        if len(cat_value) <= 3:
            return cat_value.upper()
        return cat_value.title()

    def _rename_category(cat_value):
        _key = clean_string_lower_case(cat_value)
        return _make_dict.get(_key, _cat_default_rule(cat_value))

    # replace 'YES!' with 'AUDI'.
    data['MakeText'].replace({'YES!': 'AUDI'}, inplace=True)

    # cast makeText to 'category'
    data['MakeText'] = data['MakeText'].astype('category', copy=True)
    # print('MakeText categories -- Before:', data['MakeText'].cat.categories)
    # print(len(data['MakeText'].cat.categories))

    # new categories
    data['MakeText'].cat.categories = [_rename_category(g) for g in data['MakeText'].cat.categories]
    # print('MakeText categories -- After:', data['MakeText'].cat.categories)
    # print(len(data['MakeText'].cat.categories))
    return data


def normalise_TypeName(data):
    """ Normalise TypeName attribute."""

    _TypeName_dict = {
        'BLACK SERIES': 'Black Series',
        'ARMEEFAHRZEUG': 'Army Vehicle',
        'Rennwagen': 'Racing',
        'NOVITEC': 'Novitec',
        'E 250 CGI BlueEff Avgarde': 'E 250 CGI BlueEFFICIENCY Avantgarde',
        'DAUPHINE GORDINI': 'Dauphine Gordini',
#        'quattro Turbo 20V': 'Quattro Turbo 20V',
        'GallardoLP570-4CpéTecnica' : 'Gallardo LP570-4CpéTecnica',
        '760i L': '760iL'
    }
    data['TypeName'].replace(_TypeName_dict, inplace=True)
    return data


def normalise_ModelText(data):
    """ Normalise ModelText """

    data['ModelText'].fillna('', inplace=True)
    data['ModelText'] = data['ModelText'].astype(str)
    return data


def normalise_ModelTypeText(data):
    """ Normalise ModelTypeText """
    data['ModelTypeText'] = data['ModelTypeText'].astype(str)
    return data


def normalise_FuelTypeText(data):
    """ Normalise FuelTypeText """
    data['FuelTypeText'] = data['FuelTypeText'].astype('category', copy=True)
    data['FuelTypeText'] = data['FuelTypeText'].cat.rename_categories({'Benzin':'Petrol', 'Benzin/Elektro': 'Petrol/Electric'})
    data['FuelTypeText'].replace({np.nan: None}, inplace=True)
    return data


def normaliseConditionTypeText(data):
    """ Normalise ConditionTypeText """
    data['ConditionTypeText'] = data['ConditionTypeText'].astype('category', copy=True)
    data['ConditionTypeText'] = data['ConditionTypeText'].cat.rename_categories({'Vorführmodell': 'Demonstrator', 'Neu': 'New'})
    return data


def normaliseConsumptionTotalText(data):
    """ Normalise ConsumptionTotalText
     and add a new float column ConsumptionTotalPerCentKm
    """
    data['ConsumptionTotalText_'] = data['ConsumptionTotalText']
    data['ConsumptionTotalText_'] = data['ConsumptionTotalText_'].astype(str)

    def _trunc_str(str_val):
        if str_val:
            return str_val[:-8]
        else:
            return None

    data['ConsumptionTotalPerCentKm'] = data['ConsumptionTotalText_'].apply(_trunc_str)
    data['ConsumptionTotalPerCentKm'] = pd.to_numeric(data['ConsumptionTotalPerCentKm'], errors='coerce')
    data['ConsumptionTotalPerCentKm'] = data['ConsumptionTotalPerCentKm'].astype(float)
    del data['ConsumptionTotalText_']
    return data


def normaliseCo2EmissionText(data):
    """ Normalise Co2EmissionText
        Create two new attribute columns Co2Emission and Co2EmissionUnit
    """
    data['Co2EmissionText_'] = data['Co2EmissionText']
    data['Co2EmissionText_'] = data['Co2EmissionText_'].astype(str)

    def _trunc_str(str_val):
        if str_val:
            return str_val[:-5]
        else:
            return None

    data['Co2Emission'] = data['Co2EmissionText_'].apply(_trunc_str)
    data['Co2Emission'] = pd.to_numeric(data['Co2Emission'], errors='coerce')

    data['Co2EmissionUnit'] = ''
    data.loc[data['Co2EmissionText'].notnull(), ['Co2EmissionUnit']] = 'g/km'

    del data['Co2EmissionText_']
    return data


def normalise_TypeNameFull(data):
    """ Normalise TypeNameFull """
    data['TypeNameFull'] = data['MakeText'].astype(str) + ' ' + data['ModelTypeText']
    return data


def normalise_data(data, copy=False):
    """ Normalise supplier pre-processed data"""
    if copy:
        data = data.copy(deep=True)

    # int columns
    int_columns = ['ID', 'Seats', 'FirstRegYear', 'Hp', 'Doors', 'FirstRegMonth', 'Ccm', 'Km']
    data[int_columns] = data[int_columns].astype(int)

    # categorical columns
    cat_columns = ['Properties', 'City', 'ConsumptionRatingText' ]

    data = normaliseConsumptionTotalText(data)
    data = normaliseCo2EmissionText(data)
    # normalisation
    data = normalise_MakeText(data)
    data = normalise_TypeName(data)
    data = normalise_ModelText(data)
    data = normalise_TypeNameFull(data)

    data = normalise_FuelTypeText(data)
    data = normaliseConditionTypeText(data)
    return data


def pickle_normalised_data(output_dirpath, data):
    pkl_filepath = os.path.join(output_dirpath, 'normalised_data.pkl')
    joblib.dump(data, pkl_filepath)
    return pkl_filepath


def write_normalised_data(filepath, data):
    """ Write normalised data into the xlsx supplier output file."""
    _book = openpyxl.load_workbook(filepath)
    with pd.ExcelWriter(filepath, engine='openpyxl') as writer:
        writer.book = _book
        data.to_excel(writer, sheet_name='normalised_data', index=False, na_rep='')

